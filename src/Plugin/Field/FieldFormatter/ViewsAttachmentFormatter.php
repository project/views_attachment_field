<?php

namespace Drupal\views_attachment_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'views_attachment' formatter.
 *
 * @FieldFormatter(
 *   id = "views_attachment",
 *   label = @Translation("Views attachment formatter"),
 *   field_types = {
 *     "views_attachment"
 *   }
 * )
 */
class ViewsAttachmentFormatter extends FormatterBase {

  /**
   * Optional arguments to supply to the view.
   */
  const SETTINGS__VIEW_ARGUMENTS = 'view_arguments';

  /**
   * The selected view display ID.
   */
  const SETTINGS__VIEW_DISPLAY_ID = 'view_display_id';

  /**
   * The selected view ID.
   */
  const SETTINGS__VIEW_ID = 'view_id';

  /**
   * Token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->tokenService = $container->get('token');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      self::SETTINGS__VIEW_ID => '',
      self::SETTINGS__VIEW_DISPLAY_ID => '',
      self::SETTINGS__VIEW_ARGUMENTS => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState) {
    $form = parent::settingsForm($form, $formState);

    $form[self::SETTINGS__VIEW_ID] = [
      '#default_value' => $this->getSetting(self::SETTINGS__VIEW_ID),
      '#required' => TRUE,
      '#title' => $this->t('View ID'),
      '#type' => 'textfield',
    ];

    $form[self::SETTINGS__VIEW_DISPLAY_ID] = [
      '#default_value' => $this->getSetting(self::SETTINGS__VIEW_DISPLAY_ID),
      '#required' => TRUE,
      '#title' => $this->t('View display ID'),
      '#type' => 'textfield',
    ];

    $form[self::SETTINGS__VIEW_ARGUMENTS] = [
      '#default_value' => $this->getSetting(self::SETTINGS__VIEW_ARGUMENTS),
      '#title' => $this->t('View argument'),
      '#type' => 'textfield',
    ];

    // Get the entity type to provide token type.
    $fieldDefinition = $this->fieldDefinition;
    $entityTypeId = $fieldDefinition->getTargetEntityTypeId();
    $form['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$entityTypeId],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $settings = $this->getSettings();

    if (!empty($settings[self::SETTINGS__VIEW_ID])) {
      $summary[] = $this->t('View ID: <strong>@id</strong>', ['@id' => $settings[self::SETTINGS__VIEW_ID]]);
    }
    else {
      $summary[] = $this->t('⚠️ View ID: <em>️undefined</em>');
    }

    if (!empty($settings[self::SETTINGS__VIEW_DISPLAY_ID])) {
      $summary[] = $this->t('View display ID: <strong>@id</strong>', ['@id' => $settings[self::SETTINGS__VIEW_DISPLAY_ID]]);
    }
    else {
      $summary[] = $this->t('View display ID: <em>Master (default)</em>');
    }

    if (!empty($settings[self::SETTINGS__VIEW_ARGUMENTS])) {
      $summary[] = $this->t('View argument: <strong>@arg</strong>', ['@arg' => $settings[self::SETTINGS__VIEW_ARGUMENTS]]);
    }
    else {
      $summary[] = $this->t('View argument: <em>none</em>');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $viewId = $this->getSetting(self::SETTINGS__VIEW_ID);
    $viewDisplayId = $this->getSetting(self::SETTINGS__VIEW_DISPLAY_ID);
    $viewArguments = $this->getSetting(self::SETTINGS__VIEW_ARGUMENTS);

    // Retrieve the view.
    $executableView = Views::getView($viewId);

    if (empty($executableView)) {
      // View not found.
      return [];
    }

    if (!$executableView->access($viewDisplayId)) {
      // No access.
      return [];
    }

    if (empty($viewDisplayId)) {
      // Normalize the value to NULL if empty (empty string).
      $viewDisplayId = NULL;
    }

    // Retrieve the context entity for optional argument processing.
    /* @var \Drupal\Core\Entity\Plugin\DataType\EntityAdapter $entityAdapter */
    $entityAdapter = $items->getParent();
    $contextEntity = $entityAdapter->getEntity();

    // Render the view.
    $rendered = $executableView->preview($viewDisplayId, $this->processViewsArgumentsSettings($viewArguments, $contextEntity));

    return $rendered;
  }

  /**
   * Processes the views arguments formatter settings for views.
   *
   * @param mixed $viewArguments
   *   View arguments.
   * @param \Drupal\Core\Entity\EntityInterface $contextEntity
   *   Entity which is being rendered, containing the field.
   *
   * @return array
   *   An array of arguments.
   */
  protected function processViewsArgumentsSettings($viewArguments, EntityInterface $contextEntity) {
    if (empty($viewArguments)) {
      // Preemptively abort the process.
      return [];
    }

    // Transform using tokens.
    $processed = $this->tokenService->replace($viewArguments, [$contextEntity->getEntityTypeId() => $contextEntity]);

    // TODO: support multiple arguments.
    return [
      $processed,
    ];
  }

}
