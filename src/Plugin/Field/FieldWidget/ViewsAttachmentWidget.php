<?php

namespace Drupal\views_attachment_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'views_attachment' widget.
 *
 * @FieldWidget(
 *   id = "views_attachment",
 *   module = "views_attachment_field",
 *   label = @Translation("Views attachment widget"),
 *   field_types = {
 *     "views_attachment"
 *   }
 * )
 */
class ViewsAttachmentWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    return $element;
  }

}
