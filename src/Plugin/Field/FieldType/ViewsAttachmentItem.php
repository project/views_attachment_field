<?php

namespace Drupal\views_attachment_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'views_attachment' field type.
 *
 * @FieldType(
 *   id = "views_attachment",
 *   label = @Translation("Views attachment"),
 *   description = @Translation("Provides a computed field which displays a view result."),
 *   default_formatter = "views_attachment",
 *   cardinality = 1
 * )
 */
class ViewsAttachmentItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Dummy field to workaround the inability to define configurable fields
    // with a custom storage (field storage config page crashes if we leave
    // schema and property definitions empty).
    // Maybe this "value" column will be of any use later. Maybe.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Dummy string value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    // Dummy field to workaround the inability to define configurable fields
    // with a custom storage (field storage config page crashes if we leave
    // schema and property definitions empty).
    // Maybe this "value" column will be of any use later. Maybe.
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Telling it is never empty.
    // TODO: check whether this is a desired behaviour.
    return FALSE;
  }

}
