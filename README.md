INTRODUCTION
------------

Views Attachment Field module provides means to embed a view into a field
in Field UI.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/views_attachment_field

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/views_attachment_field

REQUIREMENTS
------------

This module requires the following modules:

 * Token (https://www.drupal.org/project/token)

 INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * Create / prepare the view you want to embed.
 * On the entity Field UI, add a "Views attachment" field.
 * (optional) Disable the field in the form display (this is never used).
 * In the view display, configure the field to use the view ID and display ID.
 * (optional) Set the argument, which may contain token.

Multiple arguments is not yet supported.
